<?php

namespace EyeQue\Linneo\Observer\Sales;

/**
 * Class OrderSaveAfter
 *
 * @package Ayakil\OrdersModification\Observer\Sales
 */
class OrderSaveAfter implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $productRepository;

    /**
     * @var \EyeQue\Linneo\Helper\Data
     */
    protected $_helper;

    /**
     * @param \Magento\Catalog\Model\ProductRepository
     * @param \EyeQue\Linneo\Helper\Data $helper
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \EyeQue\Linneo\Helper\Data $helper
    ) {
        $this->logger = $logger;
        $this->_helper = $helper;
        $this->productRepository = $productRepository;
    }

    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    ) {
        // Getting checkout session to get saved values.
        $cartSession = $this->_helper->getCheckoutSession();
        $formvalues = (array)$cartSession->getFormDataSession();

        $order= $observer->getData('order');
        if(array_key_exists("trackingNumber", $formvalues )) {
            $order->setTrackingNumber($formvalues['trackingNumber']);
            $order->save();
        }
        
    }
}
