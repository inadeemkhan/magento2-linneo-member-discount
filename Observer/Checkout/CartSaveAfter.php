<?php

namespace EyeQue\Linneo\Observer\Checkout;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use \Magento\Framework\Message\ManagerInterface ;
use EyeQue\Linneo\Helper\Data;

class CartSaveAfter implements ObserverInterface
{
    /**
     * @var Data
     */
    protected $_helperData;

    /**
     * @var ManagerInterface
     */
    protected $_messageManager;

    /**
     * @var \Magento\Quote\Model\Quote
     */
    protected $quote;
    /**
     * @var \EyeQue\Linneo\Model\MemberInformation
     */
    protected $_memberInformation;

    /**
     * @var string
     */
    protected $apiUrl = 'https://authorization-np.skygenusasystems.com/connect/token';
    /**
     * @var string
     */
    protected $method = 'POST';

    /**
     * Below is the method that will fire whenever the event runs!
     *
     * @param Observer                                  $observer
     * @param \Magento\Checkout\Model\Session           $checkoutSession
     * @param \EyeQue\Linneo\Helper\Data                $helper
     * @param \EyeQue\Linneo\Model\MemberInformation    $memberInformation
     */

    public function __construct(
        \Magento\Checkout\Model\Session         $checkoutSession,
        Data                                    $helperData,
        ManagerInterface                        $messageManager,
        \EyeQue\Linneo\Model\MemberInformation  $memberInformation
    ) {
        $this->_checkoutSession     = $checkoutSession;
        $this->quote                = $checkoutSession->getQuote();
        $this->_helper              = $helperData;
        $this->_messageManager      = $messageManager;
        $this->_memberInformation   = $memberInformation;
    }

    public function execute(Observer $observer)
    {

        $cart = $observer->getData('cart');
        $quote = $cart->getData('quote');

        // Getting Updated Cart subtotal.
        $subTotal = $quote->getSubtotal();

        $apiResponseFlag = 1;
        $formData = (array)$this->_checkoutSession->getFormDataSession();

        if(array_key_exists('apiRepsonse', $formData) && $formData['apiRepsonse'] == 1) {
            
            $auth_data           = $this->_helper->getAuthorizeCredentials();
            $accessTokenGenerate = $this->_memberInformation->getAccessTokenGenerate($this->method, $this->apiUrl, $auth_data);
            $accessTokenDetails  = json_decode($accessTokenGenerate, true);
            $authorizeKey        = "Authorization: Bearer ".$accessTokenDetails['access_token'];
            $tokenType           = $accessTokenDetails['token_type'];
            $authApiUrl          = "https://es-np.skygenusaapis.com/retail/v2020.4/member";

            $linneoMemberDetails = array(
                'firstName' 		=> $formData['first-name'],
                'lastName' 	        => $formData['last-name'],
                'zip' 		        => $formData['zip-code'],
                'dateOfBirth' 		=> $formData['date-of-birth'],
                'locationId' 		=> $this->_helper->getLocationId(),
                'transactionType'   => $this->_helper->getTransactionType(),
                'totalRetailPrice'  => $subTotal
            );
    
            // Checking if Subscriber Id exist in array
            if(array_key_exists('subscriber-id', $formData) && !empty($formData['subscriber-id'])) {
                $linneoMemberDetails['subscriberId'] = $formData['subscriber-id'];
            }
    
            // Checking if Member Id exist in array
            if(array_key_exists('member-id', $formData) && !empty($formData['member-id'])) {
                $linneoMemberDetails['memberId'] = $formData['member-id'];
            }

            $linneoMemberDiscount = $this->_memberInformation->getLinneoMemberDetails($authorizeKey, $this->method, $authApiUrl, json_encode($linneoMemberDetails));
            $apiResponce = (array)json_decode($linneoMemberDiscount);

            $formData['memberPaymentAmount'] = $apiResponce['memberPaymentAmount'];
            $formData['cart-total'] = $subTotal;

            if(!in_array('Hello! We cannot find a record of your membership.', $apiResponce)) {
                $apiResponseFlag = 0;
            }
    
            if($apiResponseFlag) {
                $apiResponce['apiRepsonse'] = 1;
                $totalData = array_merge($apiResponce, $formData);
            } else {
                $apiResponce['apiRepsonse'] = 0;
                $totalData = array_merge($apiResponce, $formData);
            }
    
            // After getting response, setting up all data into cart session.
            $cartSession = $this->_helper->getCheckoutSession();
            $cartSession->setFormDataSession($totalData);
            $formvalues = $cartSession->getFormDataSession();
            clearstatcache();
        }
        return;
    }
}