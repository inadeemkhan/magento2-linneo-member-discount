<?php
declare(strict_types=1);

namespace EyeQue\Linneo\Controller\Member;

use Magento\Framework\Message\ManagerInterface;

class Index extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;
    /**
     * @var \EyeQue\Linneo\Helper\Data
     */
    protected $_helper;

    /**
     * @var \EyeQue\Linneo\Model\MemberInformation
     */
    protected $_memberInformation;

    /**
     * @var string
     */
    protected $apiUrl = 'https://authorization-np.skygenusasystems.com/connect/token';
    /**
     * @var string
     */
    protected $method = 'POST';
    /**
     * @var Magento\Framework\Message\ManagerInterface
     */
    protected $_messageManager;
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $_resultJsonFactory;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \EyeQue\Linneo\Helper\Data $helper
     * @param \EyeQue\Linneo\Model\MemberInformation $memberInformation
     * @param ManagerInterface $messageManager
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \EyeQue\Linneo\Helper\Data $helper,
        \EyeQue\Linneo\Model\MemberInformation $memberInformation,
        ManagerInterface $messageManager,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_helper = $helper;
        $this->_memberInformation = $memberInformation;
        $this->_messageManager = $messageManager;
        $this->_resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $cartSession = $this->_helper->getCheckoutSession();

        $resultJson = $this->_resultJsonFactory->create();
        $apiResponseFlag = 1;
        $formData = (array)$this->getRequest()->getPost();

        $auth_data = $this->_helper->getAuthorizeCredentials();
        
        $accessTokenGenerate = $this->_memberInformation->getAccessTokenGenerate($this->method, $this->apiUrl, $auth_data);
        $accessTokenDetails = json_decode($accessTokenGenerate, true);

        $authorizeKey = "Authorization: Bearer ".$accessTokenDetails['access_token'];
        $tokenType = $accessTokenDetails['token_type'];
        $authApiUrl = "https://es-np.skygenusaapis.com/retail/v2020.4/member";

        $linneoMemberDetails = array(
            'firstName' 		=> $formData['first-name'],
            'lastName' 	        => $formData['last-name'],
            'zip' 		        => $formData['zip-code'],
            'dateOfBirth' 		=> $formData['date-of-birth'],
            'locationId' 		=> $this->_helper->getLocationId(),
            'transactionType'   => $this->_helper->getTransactionType(),
            'totalRetailPrice'  => $formData['cart-total']
        );

        $subscriberIdFlag = false;

        // Checking if Subscriber Id exist in array
        if(array_key_exists('subscriber-id', $formData) && !empty($formData['subscriber-id'])) {
            $linneoMemberDetails['subscriberId'] = $formData['subscriber-id'];
            $subscriberIdFlag = true;
        }

        // Checking if Member Id exist in array
        if(array_key_exists('member-id', $formData) && !empty($formData['member-id'])) {
            $linneoMemberDetails['memberId'] = $formData['member-id'];
        }

        $linneoMemberDiscount = $this->_memberInformation->getLinneoMemberDetails($authorizeKey, $this->method, $authApiUrl, json_encode($linneoMemberDetails));
        $apiResponce = (array)json_decode($linneoMemberDiscount);

        $isCheckoutPage = in_array("checkout", $formData);
    
        // Checking If API response is error message.
        $errorMessage = "Incorrect Linneo membership information.";
        if (array_key_exists('0', $apiResponce)) {
            $errorMessage = $apiResponce['0'];
            $apiResponseFlag = 0;
        }

        if($apiResponseFlag && $apiResponce['additionalInfo'] == 0) {
            $apiResponce['apiRepsonse'] = 1;
            $totalData = array_merge($apiResponce, $formData);
            $this->_messageManager->addSuccess(__("You applied your Linneo discount."));
        } else {
            $apiResponce['apiRepsonse'] = 0;
            if($subscriberIdFlag) {
                $apiResponce['popup'] = "Yes";
            }

            if(array_key_exists('additionalInfo', $apiResponce) && $apiResponce['additionalInfo'] == 1) {
                $errorMessage = "Please enter you Subscriber ID and Member ID.";
            }
            $totalData = array_merge($apiResponce, $formData);
            $this->_messageManager->addError(__($errorMessage));
        }

        // After getting response, setting up all data into cart session.
        $cartSession->setFormDataSession($totalData);

        if($isCheckoutPage){
            clearstatcache();
            return $resultJson->setData([
                'arrayData' => $totalData,
                'errorMessage' => $errorMessage
            ]);
        } else {
            clearstatcache();
            $this->_redirect('checkout/cart/');
        }
    }
}

