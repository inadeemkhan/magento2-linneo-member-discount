<?php
declare(strict_types=1);

namespace EyeQue\Linneo\Controller\Member;

use Magento\Framework\Message\ManagerInterface;

class Cancel extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;
    /**
     * @var \EyeQue\Linneo\Helper\Data
     */
    protected $_helper;
    /**
     * @var Magento\Framework\Message\ManagerInterface
     */
    protected $_messageManager;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \EyeQue\Linneo\Helper\Data $helper
     * @param ManagerInterface $messageManager
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \EyeQue\Linneo\Helper\Data $helper,
        ManagerInterface $messageManager
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_helper = $helper;
        $this->_messageManager = $messageManager;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $cartSession = $this->_helper->getCheckoutSession();
        $cartSession->unsFormDataSession();

        $this->_messageManager->addSuccess(__("You removed your Linneo discount."));
        $this->_redirect('checkout/cart/');
    }
}

