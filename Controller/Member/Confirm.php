<?php
declare(strict_types=1);

namespace EyeQue\Linneo\Controller\Member;

use Magento\Framework\Message\ManagerInterface;

class Confirm extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;
    /**
     * @var \EyeQue\Linneo\Helper\Data
     */
    protected $_helper;

    /**
     * @var \EyeQue\Linneo\Model\MemberInformation
     */
    protected $_memberInformation;

    /**
     * @var string
     */
    protected $apiUrl = 'https://authorization-np.skygenusasystems.com/connect/token';
    /**
     * @var string
     */
    protected $method = 'POST';
    /**
     * @var Magento\Framework\Message\ManagerInterface
     */
    protected $_messageManager;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $_resultJsonFactory;
    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \EyeQue\Linneo\Helper\Data $helper
     * @param \EyeQue\Linneo\Model\MemberInformation $memberInformation
     * @param ManagerInterface $messageManager
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \EyeQue\Linneo\Helper\Data $helper,
        \EyeQue\Linneo\Model\MemberInformation $memberInformation,
        ManagerInterface $messageManager,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_helper = $helper;
        $this->_memberInformation = $memberInformation;
        $this->_messageManager = $messageManager;
        $this->_resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultJson = $this->_resultJsonFactory->create();
        $apiResponseFlag = 0;
        $errorMessage = "";
        $formData = (array)$this->getRequest()->getPost();
        $auth_data = $this->_helper->getAuthorizeCredentials();
        
        $accessTokenGenerate = $this->_memberInformation->getAccessTokenGenerate($this->method, $this->apiUrl, $auth_data);
        $accessTokenDetails = json_decode($accessTokenGenerate, true);

        $authorizeKey = "Authorization: Bearer ".$accessTokenDetails['access_token'];
        $tokenType = $accessTokenDetails['token_type'];
        $authApiUrl = "https://es-np.skygenusaapis.com/retail/v2020.4/purchase/confirm";

        $cartSession = $this->_helper->getCheckoutSession();
        $formvalues = $cartSession->getFormDataSession();

        if(array_key_exists('trackingNumber', $formvalues)) {
            $linneoMemberDetails = array(
                'trackingNumber' => $formvalues['trackingNumber']
            );
            $isDiscountApplied = $this->_memberInformation->getMemberConfirmation($authorizeKey, $this->method, $authApiUrl, json_encode($linneoMemberDetails));
            $apiResponce = (array)json_decode($isDiscountApplied);

            if(array_key_exists("expectedPaymentAmount", $apiResponce)){
                $apiResponseFlag = 1;
            } elseif(array_key_exists("0", $apiResponce)) {
                $errorMessage = $apiResponce['0'];
            }
        }
        return $resultJson->setData([
            'status' => $apiResponseFlag,
            'message' => $errorMessage
        ]);
    }
}

