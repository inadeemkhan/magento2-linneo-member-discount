<?php

namespace EyeQue\Linneo\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Sales\Setup\SalesSetupFactory;

/**
 * Class UpgradeData
 *
 * @package Ayakil\OrdersModification\Setup
 */
class UpgradeData implements UpgradeDataInterface
{
    private $salesSetupFactory;

    /**
     * Constructor
     *
     * @param \Magento\Sales\Setup\SalesSetupFactory $salesSetupFactory
     */
    public function __construct(SalesSetupFactory $salesSetupFactory)
    {
        $this->salesSetupFactory = $salesSetupFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function upgrade(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        // if (version_compare($context->getVersion(), "1.0.1", "<")) {
        //     $salesSetup = $this->salesSetupFactory->create(['setup' => $setup]);
        //     $salesSetup->addAttribute(
        //         'order',
        //         'tracking_number',
        //         [
        //             'type' => 'varchar',
        //             'length' => 256,
        //             'visible' => false,
        //             'required' => false,
        //             'grid' => true
        //         ]
        //     );
        // }
    }
}
