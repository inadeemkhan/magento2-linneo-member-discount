<?php
declare(strict_types=1);

namespace EyeQue\Linneo\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper
{

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    public $_storeManager;
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    protected $_scopeConfig;

    const LINNEO_DISCOUNT_IS_ENABLE = 'linneo_membership/membership_discount/isenable';
    const LINNEO_DISCOUNT_CLIENT_ID = 'linneo_membership/membership_discount/client_id';
    const LINNEO_DISCOUNT_CLIENT_SECRET = 'linneo_membership/membership_discount/client_secret';
    const LINNEO_DISCOUNT_LOCATION_ID = 'linneo_membership/membership_discount/location_id';
    const LINNEO_DISCOUNT_TRANSACTION_TYPE = 'linneo_membership/membership_discount/transactionType';

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->_storeManager=$storeManager;
        $this->_checkoutSession = $checkoutSession;
        $this->scopeConfig = $scopeConfig;
        parent::__construct($context);
    }

    /**
     * @return session
     */
    public function getCheckoutSession() {
        return $this->_checkoutSession;
    } 

    /**
     * @return string
     */
    public function getTransactionType() {
        return $this->scopeConfig->getValue(
                  self::LINNEO_DISCOUNT_TRANSACTION_TYPE,
                  \Magento\Store\Model\ScopeInterface::SCOPE_STORE
              );
    } 

    /**
     * @return int
     */
    public function getLocationId() {
        return $this->scopeConfig->getValue(
                  self::LINNEO_DISCOUNT_LOCATION_ID,
                  \Magento\Store\Model\ScopeInterface::SCOPE_STORE
              );
    } 

    /**
     * @return int
     */
    public function getAuthorizeCredentials() {
        $clientId = $this->scopeConfig->getValue(
            self::LINNEO_DISCOUNT_CLIENT_ID,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        $clientSecret = $this->scopeConfig->getValue(
            self::LINNEO_DISCOUNT_CLIENT_SECRET,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        $auth_data = array(
            'client_id'     => $clientId,
            'client_secret' => $clientSecret,
            'grant_type' 	=> 'client_credentials'
        );
        return $auth_data;
    }
    
}

