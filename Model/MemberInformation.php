<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace EyeQue\Linneo\Model;

use Magento\Framework\HTTP\Client\Curl;

/**
 * @api
 * @since 100.0.2
 */
class MemberInformation
{
    /**
     * Make request
     *
     * @param string $method
     * @param string $uri
     * @param array|string $params
     *
     */
    public function getAccessTokenGenerate($method, $uri, $params = [])
    {
        $this->_ch = curl_init();
        $this->curlOption(CURLOPT_PROTOCOLS, CURLPROTO_HTTP | CURLPROTO_HTTPS | CURLPROTO_FTP | CURLPROTO_FTPS);
        $this->curlOption(CURLOPT_URL, $uri);
        if ($method == 'POST') {
            $this->curlOption(CURLOPT_POST, 1);
            $this->curlOption(CURLOPT_POSTFIELDS, is_array($params) ? http_build_query($params) : $params);
        } else {
            $this->curlOption(CURLOPT_CUSTOMREQUEST, $method);
        }

        $this->curlOption(CURLOPT_RETURNTRANSFER, 1);
        $this->_responseBody = curl_exec($this->_ch);
        $err = curl_errno($this->_ch);
        if ($err) {
            $this->doError(curl_error($this->_ch));
        }
        curl_close($this->_ch);

        return $this->_responseBody;
    }

    /**
     * Make request
     *
     * @param string $authorizeKey
     * @param string $method
     * @param string $uri
     * @param array|string $params
     *
     */
    public function getLinneoMemberDetails($authorizeKey, $method, $uri, $params = [])
    {
        $this->_ch = curl_init();
        $this->curlOption(CURLOPT_PROTOCOLS, CURLPROTO_HTTP | CURLPROTO_HTTPS | CURLPROTO_FTP | CURLPROTO_FTPS);
        $this->curlOption(CURLOPT_URL, $uri);
        if ($method == 'POST') {
            $this->curlOption(CURLOPT_POST, 1);
            $this->curlOption(CURLOPT_POSTFIELDS, is_array($params) ? http_build_query($params) : $params);
        } else {
            $this->curlOption(CURLOPT_CUSTOMREQUEST, $method);
        }

        $this->curlOption(CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorizeKey));
        $this->curlOption(CURLOPT_RETURNTRANSFER, 1);
        $this->_responseBody = curl_exec($this->_ch);
        $err = curl_errno($this->_ch);
        if ($err) {
            $this->doError(curl_error($this->_ch));
        }
        curl_close($this->_ch);

        return $this->_responseBody;
    }

    /**
     * Make request
     *
     * @param string $method
     * @param string $uri
     * @param array|string $params
     *
     */
    public function getMemberConfirmation($authorizeKey, $method, $uri, $params = [])
    {
        $this->_ch = curl_init();
        $this->curlOption(CURLOPT_PROTOCOLS, CURLPROTO_HTTP | CURLPROTO_HTTPS | CURLPROTO_FTP | CURLPROTO_FTPS);
        $this->curlOption(CURLOPT_URL, $uri);
        if ($method == 'POST') {
            $this->curlOption(CURLOPT_POST, 1);
            $this->curlOption(CURLOPT_POSTFIELDS, is_array($params) ? http_build_query($params) : $params);
        } else {
            $this->curlOption(CURLOPT_CUSTOMREQUEST, $method);
        }

        $this->curlOption(CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorizeKey));
        $this->curlOption(CURLOPT_RETURNTRANSFER, 1);
        $this->_responseBody = curl_exec($this->_ch);
        $err = curl_errno($this->_ch);
        if ($err) {
            $this->doError(curl_error($this->_ch));
        }
        curl_close($this->_ch);

        return $this->_responseBody;
    }
    

    /**
     * Set curl option directly
     *
     * @param string $name
     * @param string $value
     * @return void
     */
    protected function curlOption($name, $value)
    {
        curl_setopt($this->_ch, $name, $value);
    }
}
