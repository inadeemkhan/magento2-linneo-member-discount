<?php
namespace EyeQue\Linneo\Model\Quote\Address\Total;

class MemberDiscount extends \Magento\Quote\Model\Quote\Address\Total\AbstractTotal
{

    /**
    * @var \Magento\Framework\Pricing\PriceCurrencyInterface
    */
    protected $_priceCurrency;
    /**
     * @var \EyeQue\Linneo\Helper\Data
     */
    protected $_helper;

    /**
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency [description]
     * @param \EyeQue\Linneo\Helper\Data $helper
     */
    public function __construct(
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        \EyeQue\Linneo\Helper\Data $helper
    ) {
        $this->_priceCurrency = $priceCurrency;
        $this->_helper = $helper;
    }

    public function collect(
        \Magento\Quote\Model\Quote $quote,
        \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment,
        \Magento\Quote\Model\Quote\Address\Total $total
    ) {
        $cartSession = $this->_helper->getCheckoutSession();
        $formvalues = (array)$cartSession->getFormDataSession();
        parent::collect($quote, $shippingAssignment, $total);

        $discountAmount = 0;
        $isDiscountEqual = false;
        $label = "Linneo member discount";
        $subTotal = $quote->getSubtotal();
        if(array_key_exists('apiRepsonse', $formvalues) && $formvalues['apiRepsonse'] == 1) {
            if($formvalues['additionalInfo'] == 0) {
                $discountAmount = -(($quote->getSubtotal() - $formvalues['memberPaymentAmount'])/2);

                // Checking if 100% Discount
                if($subTotal == $formvalues['memberPaymentAmount']) {
                    $discountAmount = -$formvalues['memberPaymentAmount'];
                    $isDiscountEqual = true;
                } 
            }
        }

        $total->setDiscountDescription($label);
        if($isDiscountEqual){
            $total->setDiscountAmount($discountAmount);
            $total->setBaseDiscountAmount($discountAmount);
        } else {
            $total->setDiscountAmount(2*$discountAmount);
            $total->setBaseDiscountAmount(2*$discountAmount);
        }
        
        $total->setSubtotalWithDiscount($total->getSubtotal() + $discountAmount);
        $total->setBaseSubtotalWithDiscount($total->getBaseSubtotal() + $discountAmount);

        $total->addTotalAmount($this->getCode(), $discountAmount);
        $total->addBaseTotalAmount($this->getCode(), $discountAmount);

        return $this;
    }
}