define([   
    'jquery',
    'Magento_Ui/js/modal/modal',
    'mage/url',
    'Magento_Checkout/js/action/get-totals',
    'Magento_Ui/js/model/messageList',
    'mage/translate',
    'domReady!',
    'mage/validation'
],
function ($, modal, url, getTotalsAction, messageList, $t) {
    'use strict';
    return {
        validate: function () {
            var orderFlag = false;
            $(".loading-mask").show();
            var customurl = url.build('linneo/member/confirm');
            $.ajax({
                url: customurl,
                type: 'POST',
                dataType: 'json',
                data: {},
                complete: function(data) { 
                    var status = data.responseJSON.status; 
                    var errorMessage = data.responseJSON.message;
                    $(".loading-mask").hide(); 

                    if (status == 1) {
                        messageList.addSuccessMessage({ message: $t("Your are placing an order with Linneo Member Discount.") });
                        orderFlag = true;
                        return orderFlag;
                    } else if (errorMessage.indexOf("Purchase confirmation") >= 0) {
                        messageList.addSuccessMessage({ message: $t(errorMessage) });
                        orderFlag = true;
                        return orderFlag;
                    } else {
                        messageList.addSuccessMessage({ message: $t(errorMessage) });
                        orderFlag = false;
                        return orderFlag;
                    }
                },
                error: function (xhr, status, errorThrown) {
                    orderFlag = true;
                    return orderFlag;
                }
            });
        }
    };
}
);