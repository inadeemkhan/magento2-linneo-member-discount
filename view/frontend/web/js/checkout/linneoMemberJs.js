define(
    [
        'ko',
        'jquery',
        'uiComponent',
        'mage/url',
        'mage/calendar'
    ],
    function (ko, $, Component,url) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'EyeQue_Linneo/checkout/linneoMember'
            }
        });
    }
);