define(
    [
        'ko',
        'jquery',
        'uiComponent',
        'mage/calendar'
    ],
    function (ko, $, Component,url) {
        'use strict';

        $("#calendar_inputField").calendar({
            changeYear: true,
            changeMonth: true,
            yearRange: "1970:2050",
            buttonText: "Select Date",
        });
    }
);