<?php
declare(strict_types=1);

namespace EyeQue\Linneo\Block\Member;

use Magento\Checkout\Model\Cart as CustomerCart;

class Index extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    public $_storeManager;
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;
    /**
     * @var CustomerCart
     */
    protected $_cart;
    
    /**
     * Constructor
     *
     * @param \Magento\Framework\View\Element\Template\Context  $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param CustomerCart $cart
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Checkout\Model\Session $checkoutSession,
        CustomerCart $cart,
        array $data = []
    ) {
        $this->_storeManager=$storeManager;
        $this->_checkoutSession = $checkoutSession;
        $this->_cart = $cart;
        parent::__construct($context, $data);
    }

    /**
     * @return staring
     */
    public function getBaseUrl(){
        return $this->_storeManager->getStore()->getBaseUrl();
    }

    /**
     * @return session
     */
    public function getCheckoutSession() {
        return $this->_checkoutSession;
    } 

    /**
     * @return session
     */
    public function getSessionCheckoutClear() {
        $cartSession = $this->getCheckoutSession();
        $cartSession->unsFormDataSession();
    } 

    /**
     * @return int
     */
    public function getSubtotalHtmlCustom()
    {
        $totals = $this->_cart->getQuote()->getTotals();
        $subtotal = $totals['subtotal']['value'];
        return $subtotal;
    }

    /**
     * @return string
     */
    public function getControllerUrl()
    {
        $formvalues = (array)$this->getCheckoutSession()->getFormDataSession();

        $controllerUrl = $this->getBaseUrl().'linneo/member/index';
        if(in_array('apiRepsonse', $formvalues) && $formvalues['apiRepsonse'] == 1 && $formvalues['additionalInfo'] == 0) {
            $controllerUrl = $this->getBaseUrl().'linneo/member/cancel';
        }
        return $controllerUrl;
    }

    /**
     * @return string
     */
    public function getButtonLabel()
    {
        $formvalues = (array)$this->getCheckoutSession()->getFormDataSession();
        
        $applowAllowance = "Apply Allowance";
        if(in_array('apiRepsonse', $formvalues) && $formvalues['apiRepsonse'] == 1 && $formvalues['additionalInfo'] == 0) {
            $applowAllowance = "Cancel Allowance";
        }
        return $applowAllowance;
    }

    /**
     * @return string
     */
    public function getPopupDisplayClass()
    {
        $popupDisplay = "not-displayed";
        $formvalues = (array)$this->getCheckoutSession()->getFormDataSession();

        if((array_key_exists('additionalInfo', $formvalues) && $formvalues['additionalInfo'] == 1) || (array_key_exists('popup', $formvalues))) {
            $popupDisplay = "is-displayed";
        }
        return $popupDisplay;
    }

}

